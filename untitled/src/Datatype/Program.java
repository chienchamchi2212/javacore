package Datatype;

import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        System.out.println("khởi tạo giá trị cho đối tượng Department");
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "sale";
        System.out.println(department1);

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "makerting";
        System.out.println(department2);

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "giamdoc";
        System.out.println(department3);


        System.out.println("khởi tạo giá trị cho đối tượng position");
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.DEV;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.TEST;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.SCRUM_MASTER;

        Position position4 = new Position();
        position4.positionId = 4;
        position4.positionName = PositionName.PM;


        System.out.println("khởi tạo giá trị cho đối tượng Account");
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "chienchamchi2212@1";
        account1.userName = "chien";
        account1.fullName = "DinhKimChien";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = new Date();

        Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "chienchamchi2212@2.com";
        account2.userName = "chien2";
        account2.fullName = "DinhKimChien2";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = new Date();

        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "chienchamchi2212@3.com";
        account3.userName = "chien3";
        account3.fullName = "DinhKimChien3";
        account3.department = department3;
        account3.position = position3;
        account3.createDate = new Date();


        System.out.println("khởi tạo giá trị cho đối tượng Group");
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "group1";
        group1.account = account1;
        group1.createDate = new Date();

        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "group2";
        group2.account = account2;
        group2.createDate = new Date();

        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "group3";
        group3.account = account3;
        group3.createDate = new Date();


        System.out.println("khởi tạo giá trị cho đối tượng GroupAccount");
        Groupaccount groupaccount1 = new Groupaccount();
        groupaccount1.account = group1.account;
        groupaccount1.joinDate = new Date();

        Groupaccount groupaccount2 = new Groupaccount();
        groupaccount2.account = group2.account;
        groupaccount2.joinDate = new Date();

        Groupaccount groupaccount3 = new Groupaccount();
        groupaccount3.account = group3.account;
        groupaccount3.joinDate = new Date();


        System.out.println("khởi tạo giá trị cho đối tượng TypeQuestion");
        Typequestion typequestion1 = new Typequestion();
        typequestion1.typeId = 1;
        typequestion1.typeName = "Multiple_Choice1";

        Typequestion typequestion2 = new Typequestion();
        typequestion2.typeId = 2;
        typequestion2.typeName = "Multiple_Choice2";

        Typequestion typequestion3 = new Typequestion();
        typequestion3.typeId = 3;
        typequestion3.typeName = "Multiple_Choice3";


        System.out.println("khởi tạo giá trị cho đối tượng CategoryQuestion");
        Categoryquestion categoryquestion1 = new Categoryquestion();
        categoryquestion1.categoryId = 1;
        categoryquestion1.categoryName = "Java";

        Categoryquestion categoryquestion2 = new Categoryquestion();
        categoryquestion2.categoryId = 2;
        categoryquestion2.categoryName = "NET";

        Categoryquestion categoryquestion3 = new Categoryquestion();
        categoryquestion3.categoryId = 3;
        categoryquestion3.categoryName = "SQL";

        Categoryquestion categoryquestion4 = new Categoryquestion();
        categoryquestion4.categoryId = 4;
        categoryquestion4.categoryName = "Postman";

        Categoryquestion categoryquestion5 = new Categoryquestion();
        categoryquestion5.categoryId = 5;
        categoryquestion5.categoryName = "Ruby";


        System.out.println("khởi tạo giá trị cho đối tượng Question");
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "content1";
        question1.account = account1;
        question1.createDate = new Date();
        question1.typequestion = typequestion1;
        question1.categoryquestion = categoryquestion1;


        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "content2";
        question2.account = account2;
        question2.createDate = new Date();
        question2.typequestion = typequestion2;
        question2.categoryquestion = categoryquestion2;

        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "content3";
        question3.account = account3;
        question3.createDate = new Date();
        question3.typequestion = typequestion3;
        question3.categoryquestion = categoryquestion3;


        System.out.println("khởi tạo giá trị cho đối tượng Answer ");
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.content = "câu trả lời 1";
        answer1.question = question1;
        answer1.isCorrect = answer1.isCorrect;

        Answer answer2 = new Answer();
        answer2.answerId = 2;
        answer2.content = "Câu trả lời 2";
        answer2.question = question2;
        answer2.isCorrect = answer1.isCorrect;

        Answer answer3 = new Answer();
        answer3.answerId = 3;
        answer3.content = "Câu trả lời 3";
        answer3.question = question3;
        answer3.isCorrect = answer3.isCorrect;


        System.out.println("khởi tạo giá trị cho đối tượng  Exam");
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = 1;
        exam1.title = "tiêu đề thi 1";
        exam1.categoryquestion = categoryquestion1;
        exam1.duration = 20;
        exam1.creatorId = 1;

        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = 2;
        exam2.title = "tiêu đề thi 2";
        exam2.categoryquestion = categoryquestion2;
        exam2.duration = 20;
        exam2.creatorId = 2;

        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = 3;
        exam3.title = "tiêu đề thi 3";
        exam3.categoryquestion = categoryquestion3;
        exam3.duration = 20;
        exam3.creatorId = 3;


        System.out.println("khởi tạo giá trị cho đối tượng ExamQuestion ");
        Examquestion examquestion1 = new Examquestion();
        examquestion1.exam = exam1;
        examquestion1.question = question1;

        Examquestion examquestion2 = new Examquestion();
        examquestion2.exam = exam2;
        examquestion2.question = question2;
        Examquestion examquestion3 = new Examquestion();
        examquestion3.exam = exam3;
        examquestion3.question = question3;

        Account[] accounts1 = {account1, account2};
        group2.accounts = accounts1;
        Account[] accounts2 = {account2, account3};
        group3.accounts = accounts2;
        Account[] accounts3 = {account1};
        group1.accounts = accounts3;


        Group[] groups1 = {group1, group2};
        account2.groups = groups1;
        Group[] groups2 = {group2, group3};
        account3.groups = groups2;
        Group[] groups3 = {group1};
        account1.groups = groups3;


        Exam[] exams1 = {exam1, exam2};
        question1.exams = exams1;
        Exam[] exams2 = {exam2, exam3};
        question2.exams = exams2;
        Exam[] exams3 = {exam3};
        question3.exams = exams3;

        Examquestion[] examquestions1 = {examquestion1, examquestion2};
        exam1.questions = exam1.questions;
        Examquestion[] examquestions2 = {examquestion2, examquestion3};
        exam2.questions = exam2.questions;
        Examquestion[] examquestions3 = {examquestion3};
        exam3.questions = exam3.questions;


//        -----------------------------------------Assingment2------------------------------------
        System.out.println("-----Question1------");
        System.out.println("Kiểm tra account thứ 2");
        if (account2.department == null) {
            System.out.println("Nhân viên này chưa có phòng ban");
        } else {
            System.out.println("Phòng ban của nhân viên này là ..." + ":" + account2.department.departmentName);
        }


        System.out.println("----Question 2----");
        if (account2.groups == null) {
            System.out.println("Nhân viên này chưa có group");
        } else if (account2.groups.length == 1 || account2.groups.length == 2) {
            System.out.println("Group này của nhân viên là Java Fresher, C# Fresher");
        } else if (account2.groups.length == 3) {
            System.out.println("nhân viên này là người quan trọng, tham gia nhiều group");
        } else {
            System.out.println("nhân viên này là người hóng truyện, tham gia tất cả các group");
        }

        System.out.println("----Question 3----");
        System.out.println((account2.department == null) ? "nhân viên này chưa có phong ban"
                : "phòng ban của nhân viên này là " + ": " +
                "" + account2.department.departmentName);


        System.out.println("-----Question4-----");
        String ketqua = (account1.position.positionName == PositionName.DEV) ?
                "Đây là Developer"
                : "Người này không phải là Developer";
        System.out.println(ketqua);


        System.out.println("-----Question5----");
//        "lấy ra số lượng Account1 và in theo fomat sau"
        switch (group1.accounts.length) {
            case 1:
                System.out.println("Nhóm có một thành viên");
                break;
            case 2:
                System.out.println("Nhóm có hai thành viên");
                break;
            case 3:
                System.out.println("Nhóm có ba thành viên");
                break;
            default:
                System.out.println("Nhóm có nhiều thành viên");
                break;
        }
        System.out.println("-----Question6-----");
        System.out.println("Sử dụng switch case để làm lại Question 2");
        switch (account2.groups.length){
            case 0:
                System.out.println("nhân viên này chưa có group");
                break;
            case 1:
            case 2:
                System.out.println("Group này của nhân viên là Java Fresher, C# Fresher");
                break;
            case 3:
                System.out.println("nhân viên này là người quan trọng, tham gia nhiều group");
                break;
            default:
                System.out.println("nhân viên này là người hóng truyện, tham gia tất cả các group");
                break;

        }


        System.out.println("-----Question8-----");
        System.out.println("In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của họ");
        Account[] accounts = {account1, account2, account3, account1};
        System.out.println("Danh sach account :");
        int i = 1;
        for (Account account : accounts) {
            System.out.println("thông tin account thứ" + i);
            System.out.println("Email: " + account.email);
            System.out.println("FullName:" + account.fullName);
            System.out.println("Department:" + account.department.departmentName);
            i++;
        }

        System.out.println("-----Question9-----");
        System.out.println("In ra thông tin các phòng ban bao gồm: id và name");
        Department[] departments = {department1,department2,department3};
        System.out.println("danh sách phòng ban là");
        for (Department department: departments) {
            System.out.println("Id:"+department.departmentId);
            System.out.println("Name"+department.departmentName);
        }

        System.out.println("------Question10-----");
        System.out.println("In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của");
        System.out.println("Danh sach account :");
        for (int j = 0; j < accounts.length; j++) {
            System.out.println("thông tin Account thứ " + (j + 1) + " là :");
            System.out.println("Email: " + accounts[j].email);
            System.out.println("Fullname:" + accounts[j].fullName);
            System.out.println("Deparment name: " + accounts[j].department.departmentName);

        }

        System.out.println("------Question11------");
        System.out.println("danh sách phòng ban là");
        for (int j = 0; j < departments.length; j++) {
            System.out.println("thông tin Department thứ " + (j + 1) + " là :");
            System.out.println("id:" + departments[j].departmentId);
            System.out.println("name:" + departments[j].departmentName);

        }

        System.out.println("------Qúestion12------");
        System.out.println("hai phòng ban đầu tiên là");
//        for (int j = 0; j < 2; j++) {
//            System.out.println("thông tin Department thứ "+(j+1)+" là :");
//            System.out.println("id:"+ departments[j].departmentId);
//            System.out.println("name:"+ departments[j].departmentName);
//        }

        for (int j = 0; j < departments.length; j++) {
            if (j < 2) {
                System.out.println("thông tin Department thứ " + (j + 1) + " là :");
                System.out.println("id:" + departments[j].departmentId);
                System.out.println("name:" + departments[j].departmentName);
            }
        }

        System.out.println("-------Question13-------");
        System.out.println("thông tin các account ngoại trừ account thứ 2 là");
        for (int j = 0; j < accounts.length; j++) {
            if (j != 2) {
                System.out.println("thông tin Account thứ " + (j + 1) + " là :");
                System.out.println("Email: " + accounts[j].email);
                System.out.println("Fullname:" + accounts[j].fullName);
                System.out.println("Deparment name: " + accounts[j].department.departmentName);
            }
        }

        System.out.println("-------Question14-------");
        System.out.println("thông tin các account có id < 2");
        for (int j = 0; j < accounts.length; j++) {
            if (accounts[j].accountId < 2) {
                System.out.println("thông tin Account thứ " + (j + 1) + " là :");
                System.out.println("id: " + accounts[j].accountId);
                System.out.println("Email: " + accounts[j].email);
                System.out.println("Fullname:" + accounts[j].fullName);
                System.out.println("Deparment name: " + accounts[j].department.departmentName);
            }
        }

        System.out.println("--------Question15-------");
        System.out.println("các số chẵn < hoặc = 20");
//        for (int j = 1; j <= 20; j++) {// j++ <> j= j+1
//            if (j % 2 == 0) {
//                System.out.println(j);
//            }
//        }

        for (int j = 2; j <= 20; j = j + 2) {  // j+= 2  <> j=j+2
            System.out.println(j);
        }

        System.out.println("--------Question16.10--------");
        System.out.println("Danh sasch account la:");
        int a = 0;
        while (a < accounts.length) {
            System.out.println("thông tin Account thứ " + (a + 1) + " là :");
            System.out.println("Email: " + accounts[a].email);
            System.out.println("Fullname:" + accounts[a].fullName);
            System.out.println("Deparment name: " + accounts[a].department.departmentName);
            a = a + 1;
        }

        






















    }
}





